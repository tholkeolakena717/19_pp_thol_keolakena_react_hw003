import React, { Component } from 'react'


export default class Tables extends Component {
 

  constructor() {
    super();
    this.state = {
      status:"",
      color:""
    };

  }
handleSubmit = () => {
    this.setState({
     status:"dance",
     
     
   });
  };
 
  render() {
    return (
      
        <table class=" nth-table mt-5 w-3/4 text-sm text-left text-gray-500 dark:text-gray-400 ml-28">
        <thead class=" text-xs text-gray-700 uppercase bg-black dark:bg-gray-500 dark:text-gray-400">
          <tr>
            <th scope="col" class="px-6 py-3  ">
            <u>ID</u>
            </th>
            <th scope="col" class="px-6 py-3">
            <u> Email</u> 
            </th>
            <th scope="col" class="px-6 py-3  ">
            <u>Username</u>
            </th>
            <th scope="col" class="px-6 py-3">
               <u>Age</u>
            </th>
            <th scope="col" class="px-6 py-3  ">
            <u>Action</u>
            </th>
          </tr>
        </thead>
        <tbody>
          {this.props.allstundent.map((item) => (
            <tr key={item.id} class="border-b bg-gray-50 dark:bg-gray-800 dark:border-gray-700">
              <td class="px-6 py-4">{item.id}</td>
              <td class="px-6 py-4">{item.Email}</td>
              <td class="px-6 py-4">{item.name}</td>
              <td class="px-6 py-4">{item.age}</td>
              <td class="px-6 py-4"> <button onClick={this.handleSubmit} class="mt-12 w-44 h-11 bg-orange-500 rounded-sm border-2 border-indigo-600  rounded-xl hover:bg-red-500  "> 
     
      {this.state.status==""?item.status:this.state.status}
     </button>
     <button onClick={this.handleSubmit}  class="mt-12 w-44 h-11 bg-orange-500 rounded-sm border-2 border-indigo-600  rounded-xl hover:bg-red-500  "> 
     {item.status1}
     </button></td>

              

            </tr>
          ))}
        </tbody>
      </table>
    )
  }
}
